package com.alphatica.genotick.data;

public interface DataLoader {
    String DEFAULT_DATA_DIR = "data";

    MainAppData createRobotData();
}
